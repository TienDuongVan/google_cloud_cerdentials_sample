//// Copyright 2018 Google LLC
////
//// Licensed under the Apache License, Version 2.0 (the "License");
//// you may not use this file except in compliance with the License.
//// You may obtain a copy of the License at
////
////     http://www.apache.org/licenses/LICENSE-2.0
////
//// Unless required by applicable law or agreed to in writing, software
//// distributed under the License is distributed on an "AS IS" BASIS,
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//// See the License for the specific language governing permissions and
//// limitations under the License.
//
//// [START drive_quickstart]
//import com.google.api.client.auth.oauth2.Credential;
//import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
//import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
//import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
//import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
//import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
//import com.google.api.client.http.javanet.NetHttpTransport;
//import com.google.api.client.json.JsonFactory;
//import com.google.api.client.json.jackson2.JacksonFactory;
//import com.google.api.client.util.store.FileDataStoreFactory;
//import com.google.api.services.drive.Drive;
//import com.google.api.services.drive.DriveScopes;
//import com.google.api.services.drive.model.File;
//import com.google.api.services.drive.model.FileList;
//
//import java.io.*;
//import java.security.GeneralSecurityException;
//import java.util.Collections;
//import java.util.List;
//
//public class DriveQuickstart {
//	private static final String APPLICATION_NAME = "Google Drive API Java Quickstart";
//	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
//	private static final String TOKENS_DIRECTORY_PATH = "tokens";
//
//	/**
//	 * Global instance of the scopes required by this quickstart.
//	 * If modifying these scopes, delete your previously saved tokens/ folder.
//	 */
//	private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE_METADATA_READONLY);
//	private static final String CREDENTIALS_FILE_PATH = "rosy-sky-293908-ec40ab6f4224.json";
//
//	/**
//	 * Creates an authorized Credential object.
//	 * @param HTTP_TRANSPORT The network HTTP Transport.
//	 * @return An authorized Credential object.
//	 * @throws IOException If the credentials.json file cannot be found.
//	 */
//	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
//		// Load client secrets.
//		InputStream in = new FileInputStream("C:\\Users\\TranQuangTruong\\Desktop\\Authen demo anh hieu\\authorize_google\\client_secret_326090875494-223e7bhg5revvs9s33qohtgqihqnjoav.apps.googleusercontent.com.json");// DriveQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
//		if (in == null) {
//			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
//		}
//		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
//
//		// Build flow and trigger user authorization request.
//		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
//				HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
//				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
//				.setAccessType("offline")
//				.build();
//		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8080).setCallbackPath("/callback").build();
//		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
//	}
//
//	public static void main(String... args) throws IOException, GeneralSecurityException {
//		// Build a new authorized API client service.
//		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
//		Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
//				.setApplicationName(APPLICATION_NAME)
//				.build();
//
//		// Print the names and IDs for up to 10 files.
//		FileList result = service.files().list()
//				.setPageSize(10)
//				.setFields("nextPageToken, files(id, name)")
//				.execute();
//		List<File> files = result.getFiles();
//		if (files == null || files.isEmpty()) {
//			System.out.println("No files found.");
//		} else {
//			System.out.println("Files:");
//			for (File file : files) {
//				System.out.printf("%s (%s)\n", file.getName(), file.getId());
//			}
//		}
//	}
//}
//// [END drive_quickstart]


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.ServiceAccountCredentials;


import java.io.*;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.List;

public class DriveQuickstart {
	private static final String APPLICATION_NAME = "your awesome app";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private static final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE);
	private static final String CREDENTIALS_FILE_PATH = "C:\\Users\\TranQuangTruong\\Desktop\\java-docs-samples\\google_drive_quickStart\\rosy-sky-293908-ec40ab6f4224.json";

	public static void main(String... args) throws IOException, GeneralSecurityException {
		// Build a new authorized API client service
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		HttpRequestInitializer requestInitializer = new HttpCredentialsAdapter(ServiceAccountCredentials.fromStream(new FileInputStream(CREDENTIALS_FILE_PATH))
				.createScoped(SCOPES)
				.createDelegated("drive-leak@rosy-sky-293908.iam.gserviceaccount.com"));


		System.out.println(requestInitializer);
		Drive driveService = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, requestInitializer)
				.setApplicationName(APPLICATION_NAME)
				.build();


		File fileMetadata = new File();
		fileMetadata.setName("My Report");
		fileMetadata.setMimeType("application/vnd.google-apps.spreadsheet");

//		java.io.File filePath = new java.io.File("C:\\Users\\TranQuangTruong\\Desktop\\java-docs-samples\\google_drive_quickStart\\rosy-sky-293908-ec40ab6f4224.json");
//		FileContent mediaContent = new FileContent("text/csv", filePath);
//		File fileUpload = driveService.files().create(fileMetadata, mediaContent)
////				.setFields("id")
//				.setFields("id, webContentLink, webViewLink, parents")
//				.execute();
//
//		System.out.println("File ID: " + fileUpload.getId() + "link: " + fileUpload.getWebContentLink() + "\n" + fileUpload.getWebViewLink());


		// Print the names and IDs for up to 10 files.
		FileList result = driveService.files().list()
				.setPageSize(10)
				.setFields("nextPageToken, files(id, name)")
				.execute();

		List<File> files = result.getFiles();
		if (files == null || files.isEmpty()) {
			System.out.println("No files found.");
		} else {
			System.out.println("Files:");
			for (File file : files) {
				System.out.printf("%s (%s) %s\n", file.getName(), file.getId(), file.getMimeType());
			}
		}

		String fileId = "1d25q8v3vawZqCiyVbFJVLESC43jgBt9QzrpyOyJRVRY";
//		OutputStream outputStream = new ByteArrayOutputStream();
//		driveService.files().export(fileId, "application/vnd.google-apps.spreadsheet")
//				.executeMediaAndDownloadTo(outputStream);


//		String fileId = "0BwwA4oUTeiV1UVNwOHItT0xfa2M";
//		OutputStream outputStream = new FileOutputStream("E:\\New folder\\ok.json");
//
//		driveService.files().get(fileId).setAlt("media")
//				.executeMediaAndDownloadTo(outputStream);

		String permissionType  = "anyone";
		String permissionRole = "writer";
		Permission newPermission = new Permission();
		newPermission.setType(permissionType);
		newPermission.setRole(permissionRole);

//		Drive driveServices = GoogleDriveUtils.getDriveService();
		driveService.permissions().create(fileId, newPermission).execute();
		About about = driveService.about().get().setFields("storageQuota").execute();
		System.out.println(about.getStorageQuota().getUsageInDrive());



		System.out.println(driveService.files().get(fileId).execute().getWebContentLink());
	}



	}